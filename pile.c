#include <stdio.h>
#include <stdlib.h> 

#include "pile.h"

pile_t * initPile(int taille)
{
    pile_t * p = (pile_t *) malloc(sizeof(pile_t));

    if(p)
    {
        p->taille = taille;
        p->sommet = -1;
        p->base = (elt_type *) malloc(sizeof(elt_type)*taille);

        if (p->base == NULL)
        {
            free(p);
            p = NULL;
        }  
    } 

    return p;
} 

int estVidePile(pile_t * p)
{
    return p->sommet == -1;
} 

int estPleinePile(pile_t * p)
{
    return (p->sommet + 1 >= p->taille);
} 

void libererPile(pile_t ** p)
{
    free((*p)->base);
    free(*p);
    *p = NULL;
} 

void empilerPile(pile_t * p, elt_type val)
{
    p->sommet++;
    p->base[p->sommet] = val; 
} 

elt_type depilerPile(pile_t * p)
{
    elt_type ret;
    
    ret = p->base[p->sommet];
    p->sommet--;

    return ret;
} 

elt_type sommetPile(pile_t * p)
{
    return p->base[p->sommet]; 
} 
 