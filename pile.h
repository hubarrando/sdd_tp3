#ifndef f_pile
#define f_pile

#include "arbre.h" 

typedef arbre_t * elt_type; 

typedef struct pile
{
    int taille;
    int sommet;
    elt_type * base;
} pile_t;



pile_t * initPile(int taille);
int estVidePile(pile_t * p);
int estPleinePile(pile_t * p);
void libererPile(pile_t ** p);
void empilerPile(pile_t * p, elt_type val);
elt_type depilerPile(pile_t * p);
elt_type sommetPile(pile_t * p);


#endif