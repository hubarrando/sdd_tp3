#include <stdio.h>
#include <stdlib.h>

#include "arbre.h"



void lireFichier(arbre_t ** t,char * nomfichier)
{
    int val;
    FILE * fichier = fopen(nomfichier,"r");
    if (fichier)
    {
        while (fscanf(fichier,"%d",&val) == 1)  // "%d" a changer si on change le typeval
        {
            insererVal(t,val);
        }
        fclose(fichier); 
    } 
} 

void insererVal(arbre_t ** t,typeval val)
{
    
    
    arbre_t * prec;
    arbre_t * nouv = (arbre_t *) malloc(sizeof(arbre_t));
    if (nouv)
    {
        nouv->val = val;
        nouv->fgauche = NULL;
        nouv->fdroit = NULL;
        if (*t)
        {
            
            prec = recherchePrec(*t,val);
            if (nouv->val < prec->val)
            {
                prec->fgauche = nouv;
            }
            else
            {
                prec->fdroit = nouv;
            } 
        } 
        else
        {
            *t = nouv;
        }  

    } 
    else
    {
        printf("erreur plus de place pour allouer\n");
        libererArbre(t);
        exit(1);
    } 
}

arbre_t * recherchePrec(arbre_t * t,typeval val)
{
    arbre_t * cour = t;
    while ((cour->val > val && cour->fgauche != NULL) || (cour->val < val && cour->fdroit != NULL))
    {
        if (cour->val > val)
        {
            cour = cour->fgauche;
        } 
        else
        {
            cour = cour->fdroit;
        } 
    } 
    return cour;
} 

void afficheREC(arbre_t * t)
{
    if (t)
    {
        afficheREC(t->fgauche);
        printf("%d ",t->val);
        afficheREC(t->fdroit);
    }  
} 

void libererArbre(arbre_t ** t)
{
    arbre_t * cour = *t;
    arbre_t * temp;
    pile_t * pile = initPile(MAX);
    while (cour != NULL || !estVidePile(pile))
    {
        if (cour != NULL)
        {
            if (cour->fdroit != NULL)
            {
                empilerPile(pile,cour->fdroit);
            }
            temp = cour;
            cour = cour->fgauche;
            free(temp);
        } 
        else
        {
            cour = depilerPile(pile);
        } 
    }
    *t = NULL;
    libererPile(&pile); 
} 

void afficheITE(arbre_t * t)
{
    arbre_t * cour = t;
    pile_t * pile = initPile(MAX);
    while (cour != NULL || !estVidePile(pile))
    {
        if (cour != NULL)
        {
            empilerPile(pile,cour);
            cour = cour->fgauche;
        } 
        else
        {
            cour = depilerPile(pile);
            printf("%d ",cour->val);
            cour = cour->fdroit;
        } 
    } 
    libererPile(&pile);
} 

arbre_t * rechercheVal(arbre_t * t,typeval val)
{
    arbre_t * cour = t;
    arbre_t * prec = t;
    while (cour != NULL && cour->val != val)
    {
        prec = cour;
        if (cour->val < val)
        {
            cour = cour->fdroit;
        } 
        else
        {
            cour = cour->fgauche;
        } 

    } 
    if (cour->val != val)
    {
        prec = NULL;
    } 
    return prec;
}  

void suppVal(arbre_t * t,typeval val)
{
    arbre_t * pere;
    arbre_t * fils;
    arbre_t * feuille;
    arbre_t ** cotedupere;
    pere = rechercheVal(t,val);
    if (pere == NULL)
    {
        printf("valeur a supprimer absente de l'arbre\n");
    } 
    else
    {
        if (pere->val > val)    //l'arbre est trie donc fils a gauche si pere plus grand
        {
            cotedupere = &(pere->fgauche);
            
        } 
        else
        {
            cotedupere = &(pere->fdroit);
        } 
        fils = *cotedupere;
        if (fils->fdroit != NULL)
        {
            *cotedupere = fils->fdroit;
            feuille = recherchePrec(*cotedupere,(fils->fgauche)->val); //donne la feuille tout a
                                                         // gauche du sous arbe droit du fils
            feuille->fgauche = fils->fgauche;
        } 
        else
        {
            *cotedupere = fils->fgauche;
        } 
    } 
} 

int hauteurArbre(arbre_t * t)
{
    arbre_t * cour = t;
    pile_t * pile = initPile(MAX);
    int hmax = 0;
    int hbranche = 0;
    int i = -1;
    int tab[MAX];
    while (cour != NULL || !estVidePile(pile))
    {
        if (cour != NULL)
        {

            if (cour->fdroit != NULL)
            {
                empilerPile(pile,cour->fdroit);
                i++;
                tab[i] = hbranche + 1; 
            } 
            cour = cour->fgauche;
            hbranche++;
        }
        else
        {
            if (hbranche > hmax)
            {
                hmax = hbranche;
            } 
            hbranche = tab[i];
            cour = depilerPile(pile);
            i--; 
        }   
    } 
    if (hbranche > hmax)                // derniere branche non traitee dans la boucle 
    {
        hmax = hbranche;
    } 
    libererPile(&pile);
    return hmax;
}  