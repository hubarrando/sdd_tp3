#include "pile.h" 

#ifndef f_arbre
#define f_arbre

#define MAX 100

typedef int typeval;

typedef struct arbre
{
    typeval val;
    struct arbre * fdroit;
    struct arbre * fgauche;
} arbre_t;

 

void lireFichier(arbre_t ** t,char * nomfichier);
void insererVal(arbre_t ** t,typeval val);
arbre_t * recherchePrec(arbre_t * t,typeval val);
void afficheREC(arbre_t * t);
void libererArbre(arbre_t ** t);
void afficheITE(arbre_t * t);
arbre_t * rechercheVal(arbre_t * t,typeval val);
void suppVal(arbre_t * t,typeval val);
int hauteurArbre(arbre_t * t);

#endif
