#include <stdio.h>
#include "arbre.h" 

int main()
{
    arbre_t * t = NULL;
    lireFichier(&t,"test.txt");
    printf("%d\n",hauteurArbre(t));
    afficheITE(t);
    printf("\n");
    libererArbre(&t);
    afficheITE(t);
    printf("\n");
    return 0;
} 